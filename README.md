# My 1st Linux rice

## Stuff I used

* i3-gaps-git
* i3blocks-gaps-git
* Vim (NerdTree, LaTeX-Box, vim-airline, gruvbox)
* rofi
* ncmpcpp, mpd
* zsh (oh-my-zsh, cmatrix, pipes, tty-clock)
* htop, gtop
*ranger


## Scrots  :camera:

Floating terminal, zsh, oh-my-zsh, powerlevel9k, powerline font hacker.


Rofi basic built-in theme.


True to /r/unixporn.


Fake busy fashionable scrot.


Some cool scripts (pukeskull, 3spooky, hack-exe)


Not that fake busy LaTeX fluid mechanics homework.

![2018-05-20-233201_1366x768_scrot](/uploads/fce61f376f09697b7f14f743aeb31cc8/2018-05-20-233201_1366x768_scrot.png)


Some compton transparency for the fancy duties.

![2018-05-20-233201_1366x768_scrot](/uploads/fce61f376f09697b7f14f743aeb31cc8/2018-05-20-233201_1366x768_scrot.png)

